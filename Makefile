ROOTCXXFLAGS=$(shell root-config --cflags)
ROOTLIBS=$(shell root-config --glibs)
FJCXXFLAGS=$(shell fastjet-config --cxxflags)
FJLIBS=$(shell fastjet-config --libs) -lNsubjettiness

all: clean jetReco jetAna

%: %.cxx
	$(CXX) $< $(CPPFLAGS) $(LDFLAGS) $(ROOTCXXFLAGS) $(ROOTLIBS) $(FJCXXFLAGS) $(FJLIBS) -o $@

clean:
	@rm -f jetReco jetAna
