// STL
#include <iostream>
#include <vector>

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TH1D.h"
#include "TH2D.h"
#include "THStack.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TStyle.h"
#include "TUUID.h"
#include "TApplication.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TRandom3.h"

// Fastjet
#include "fastjet/ClusterSequence.hh"

#define MATH_PI 3.14159265359


struct Event {
    std::vector<double>* cluster_pt  = nullptr;
    std::vector<double>* cluster_eta = nullptr;
    std::vector<double>* cluster_phi = nullptr;
    std::vector<double>* cluster_m   = nullptr;
};

void set_address(TTree* tree, Event& event) {
    tree->SetBranchAddress("cluster_pt"  , &event.cluster_pt);
    tree->SetBranchAddress("cluster_eta" , &event.cluster_eta);
    tree->SetBranchAddress("cluster_phi" , &event.cluster_phi);
    tree->SetBranchAddress("cluster_m"   , &event.cluster_m);
}

void get_event(TTree* tree, const uint64_t entry) {
    tree->GetEntry(entry);
}

std::vector<fastjet::PseudoJet> make_pseudojets(const Event& event) {
    // Takes the clusters in the event, and creates
    // a vector of objects of type fastjet::PseudoJet
    // which is required by the fastjet package

    // We get the clusters in spherical coordinates
    // (pt, eta, phi, m) but the constructor of
    // fastjet::PseudoJet takes the arguments
    // in cartesian coordinates (px, py, pz, e)
    // so we use the lorentz vector p4 to do
    // the conversion for us
    TLorentzVector p4;

    // The return vector which will contain all our pseudojets
    std::vector<fastjet::PseudoJet> pjets;

    // Loop through all clusters in the event
    for (size_t i = 0; i < event.cluster_pt->size(); ++i) {
        // Set the components of the lorentz vector
        // in spherical coordinates
        p4.SetPtEtaPhiM(
                event.cluster_pt->at(i),
                event.cluster_eta->at(i),
                event.cluster_phi->at(i),
                event.cluster_m->at(i));

        // Append a newly created PseudoJet object at the end of the
        // return vector, letting p4 do the coordinate transformation
        // for us
        pjets.push_back(fastjet::PseudoJet(p4.Px(), p4.Py(), p4.Pz(), p4.E()));

        // To keep track of the clusters later, we give them a user index
        // equal to the index in the return vector
        pjets.back().set_user_index(i);
    }

    // Return the PseudoJets
    return pjets;
}

void add_radiation(std::vector<fastjet::PseudoJet>& pjets) {
    // To test the resilience to semi-soft radiation (such as pile-up), we add radiation
    // isotropically to the collection of pseudojets

    // The amount of radiation, 6000 approximates a mean number
    // of interactors per bunch crossing of 40
    int N = 6000;

    for (int i = 0; i < N; ++i) {
        // Draw the transverse momentum between 1 MeV and 20 GeV from a
        // flat distribution in 1/pT
        double pt = 1.e3/gRandom->Uniform(1./20, 1/0.001);

        // Draw the phi coordinate for the radiation from a flat distribution
        // between +-PI
        double phi = gRandom->Uniform(-MATH_PI, MATH_PI);

        // Draw the pseudorapidity of the radiaion from a flat distribution
        // between +-3
        double eta = gRandom->Uniform(-3., 3.);

        // Use a lorentz vector to do the coordinate transformation
        // for us, set the direction to (eta, phi), the mass to 0, and
        // the transverse momentum to pt
        TLorentzVector p4;
        p4.SetPtEtaPhiM(pt, eta, phi, 0.);

        // Add the radiation to the pseudo jets
        pjets.push_back(fastjet::PseudoJet(p4.Px(), p4.Py(), p4.Pz(), p4.E()));

        // Give it a user index so we can keep track of it
        pjets.back().set_user_index(pjets.size()-1);
    }
}

void pad_clusters(std::vector<fastjet::PseudoJet>& pjets) {
    // To make it easier to see the clusters inside
    // each jet, we padd the pseudo jets with clusters
    // with negligiable energy so they show up in the plots

    TLorentzVector p4;
    double deta = 0.025;
    double dphi = 0.025;
    for (double eta = -3. + deta/2.; eta < 3.; eta += deta) {
        for (double phi = -MATH_PI + dphi/2.; phi < MATH_PI; phi += dphi) {
            p4.SetPtEtaPhiM(1e-9, eta, phi, 0);
            pjets.push_back(fastjet::PseudoJet(p4.Px(), p4.Py(), p4.Pz(), p4.E()));
        }
    }
}

void plot_clusters(std::vector<fastjet::PseudoJet>& jets) {
    // Plot the clusters in a 2d lego plot, where each
    // jet gets its own colour

    // This is the color palette we use, if you want you can try
    // different colors from https://root.cern.ch/doc/master/pict1_TAttFill_002.png
    int palette[] = {kRed, kGreen, kBlue, kYellow, kCyan, kMagenta, kRed-2,
                    kGreen+3, kBlue-10, kYellow+3, kCyan+3, kMagenta+3, kGray};
    int ncolors = sizeof(palette)/sizeof(palette[0]);

    gStyle->SetPalette(ncolors, palette);

    // Mapping from fastjet phi to atlas phi
    auto fj2atl = [](const double& phi) { return phi > MATH_PI ? phi - 2*MATH_PI : phi; };

    auto stack = new THStack("stack", "");

    auto leg = new TLegend(0.05, 0.85, 0.3, 0.95);
    leg->SetFillColor(0);
    leg->SetFillStyle(0);
    leg->SetBorderSize(0);

    int i = 0;
    for (const auto& jet : jets) {
        auto h = new TH2D(TUUID().AsString(), "", 75, -3, 3, 75, -MATH_PI, MATH_PI);
        for (const auto& c : jet.constituents()) {
            h->Fill(c.eta(), fj2atl(c.phi()), c.pt()/1.e3);
        }
        h->SetLineWidth(1);
        h->SetLineColor(gStyle->GetColorPalette(i < ncolors ? i : ncolors-1) + 1);
        h->SetFillColor(gStyle->GetColorPalette(i < ncolors ? i : ncolors-1));
        stack->Add(h);

        // Add the three hardest jet to the legend
        if (i < 3) {
            leg->AddEntry(h, Form("p_{T} = %.1f GeV", jet.pt()/1.e3), "f");
        }

        ++i;
    }

    auto canvas = new TCanvas(TUUID().AsString(), TUUID().AsString(), 1200, 900);
    canvas->SetTheta(55);
    canvas->SetPhi(32);

    stack->Draw("fb nostack 0lego1");

    leg->Draw();

    stack->GetHistogram()->GetXaxis()->SetTitle("#eta");
    stack->GetHistogram()->GetYaxis()->SetTitle("#phi");
    stack->GetHistogram()->GetZaxis()->SetTitle("Cluster p_{T} [GeV]");

    stack->GetHistogram()->GetXaxis()->CenterTitle();
    stack->GetHistogram()->GetYaxis()->CenterTitle();

    stack->GetHistogram()->GetXaxis()->SetTitleOffset(1.5);
    stack->GetHistogram()->GetYaxis()->SetTitleOffset(1.5);
    stack->GetHistogram()->GetZaxis()->SetTitleOffset(1.5);

}

void print_jets(const std::vector<fastjet::PseudoJet>& jets, bool verbose = false) {
    // Print the pt, eta, phi, and mass of the jets. If verbose == true, also
    // print the jet constituents

    for (const auto& jet : jets) {
        printf("pt = %f GeV, eta = %f, phi = %f, mass = %f GeV\n",
                jet.pt()/1.e3, jet.eta(), jet.phi(), jet.m()/1.e3);
        if (verbose) {
            for (const auto& c : jet.constituents()) {
                if (c.user_index() == -1) continue; // Skip constituents with index = -1, these are from our padding
                printf("     id: %5i  pt = %f GeV, eta = %f, phi = %f\n",
                        c.user_index(), c.pt()/1.e3, c.eta(), c.phi());
            }
        }
    }
    std::cout << "njets = " << jets.size() << '\n';
}

void cluster_jets(const Event& event) {

    // Convert the clusters in the event to PseudoJets
    std::vector<fastjet::PseudoJet> pjets = make_pseudojets(event);

    // Pad the pseudo jets with negligiable energy for visualization
    pad_clusters(pjets);

    // The radius parameter for the jet algorithm
    // If you want you can change this to see what happens, another
    // common choice is 0.4, however 1.0 is easier for visualization
    const double R = 1.0;

    // Which jet algorihtm to use
    // anti-kt:          fastjet::antikt_algorithm
    // kt:               fastjet::kt_algorithm
    // Cambridge/Aachen: fastjet::cambridge_algorithm
    const fastjet::JetAlgorithm        jet_algorithm = fastjet::antikt_algorithm;

    // The recombination scheme specifies how the momenta of the
    // pseudo jets are combined. With E-scheme, the 4-vectors
    // of the pseudo jets are simply added. This is the standard in ATLAS
    const fastjet::RecombinationScheme recomb_scheme = fastjet::E_scheme;

    // Different strategies are implemented for the clustering algorithm,
    // when specifying Best, the strategy is choosen automatically
    const fastjet::Strategy            strategy      = fastjet::Best;

    // The JetDefinition object holds all the information about which
    // algorithm to use, the radious parameters, the recombination scheme,
    // and strategy. This is used by ClusterSequence to perform the
    // jet clustering
    fastjet::JetDefinition jet_def(jet_algorithm, R, recomb_scheme, strategy);

    // Perform the actual clustering
    fastjet::ClusterSequence cs(pjets, jet_def);

    // Get all jets from the clustering step with pT > x MeV
    const double x = 7.e3; // MeV
    std::vector<fastjet::PseudoJet> jets = fastjet::sorted_by_pt(cs.inclusive_jets(x));

    // Plot the jets with their clusters
    plot_clusters(jets);

    // Print the jets
    print_jets(jets, true);

    // ---- add pileup to the jets and plot the result ----
    // add_radiation(pjets);
    // fastjet::ClusterSequence cs2(pjets, jet_def);
    // std::vector<fastjet::PseudoJet> jets2 = fastjet::sorted_by_pt(cs2.inclusive_jets(x));
    // plot_clusters(jets2);
    // print_jets(jets2, true);

}

int main(int argc, const char* argv[]) {

    if (argc < 2) {
        printf("Usage: %s <inputfile>\n", argv[0]);
        return 1;
    }

    TApplication app("app", 0, 0);

    gStyle->SetOptStat(0);
    gStyle->SetTitleFont(42,    "xyz");
    gStyle->SetTitleSize(0.045, "xy");
    gStyle->SetTitleSize(0.030, "z");
    gStyle->SetLabelFont(42,    "xyz");
    gStyle->SetLabelSize(0.030, "xyz");
    gStyle->SetLegendFont(42);

    gRandom->SetSeed(0);

    // Open the input file and fetch the tree containing event data
    TFile f(argv[1], "READ");
    TTree* tree = dynamic_cast<TTree*>(f.Get("nominal"));

    // Create an event object to hold the
    // cluster data, and attach it to the
    // data tree
    Event event;
    set_address(tree, event);

    // Get the specified event from the data tree
    get_event(tree, 10);

    // Perform the clustering and plot the results
    cluster_jets(event);

    // for (size_t i = 0; i < 10; ++i) {
    //     get_event(tree, i);
    //     cluster_jets(event);
    // }

    app.Run();
}
