# UuJetRecoProject
The Uppsala ATLAS Project for the summer school "Diversity in the Cultures of Physics".

Revised for August 2019.

## Login to Bestlapp

### Windows
To login from a windows machine we need to first download MobaXterm: <http://download.mobatek.net/10220170312132617/MobaXterm_Installer_v10.2.zip>

Save this file somewhere, extract the contents, and run the installer. After installation you should find the app either in the start menu or on the desktop. Start the app, and you should see a button in the middle of the window that says "Start local terminal", click it, and follow the instructions for Linux/Mac.

### Linux/Mac
Open a new terminal and run the command:
```bash
$ ssh -X summerschool@bestlapp.physics.uu.se
```
You will be asked for a password, which you should have received.

 If the login was successful, navigate to the folder `UuJetRecoProject`, which should be located in your home directory.
```bash
$ cd ~/UuJetRecoProject
```

The rest of the instructions will assume that you are in this directory, unless explicitly stated otherwise.

## Compiling and running the code
### Setting up the environment
The first step is to setup the right versions of `ROOT`, `gcc`, and `fastjet`. This is taken care of by sourcing the `setup.sh` script:
```bash
$ source setup.sh
```
This needs to be done every time you which to compile or run the code in a new terminal.

If everything worked, you can verify that you have the right versions with the commands `gcc --version`, `root-config --version`, and `fastjet-config --version`. The output should be:
```bash
$ gcc --version
gcc (GCC) 4.9.3
Copyright (C) 2015 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

$ root-config --version
6.04/16
$ fastjet-config --version
3.3.0
```

### Compiling
We compile the code using GNU Make. The compilation options are stored in the file `Makefile`, but you shouldn't have to change anything in this. To compile the code, simply run the `make` command:
```bash
$ make
```
If you see no errors or warnings when running this command, the compilation was successful.

### Running
The compilation step should have produced the binary file `jetReco` which contains the main program. The program takes one input argument, the file with the input data. One should exist in the current directory, named `mc15_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.merge.DAOD_JETM8.e3668_s2576_s2132_r7267_r6282_p2528.root`. To run the program with this input, use the command (you can use auto-completion with the Tab key):
```bash
$ ./jetReco mc15_13TeV.361024.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ4W.merge.DAOD_JETM8.e3668_s2576_s2132_r7267_r6282_p2528.root
```

You should see some output in the terminal, and eventually a plot.

### Saving plots
If you have a plot open, you can save it by choosing the "Save as" option in the "File" menu. This will save it as a pdf (or png if you wish) file in the current directory (probably `~/UuJetRecoProject`). To download all the pdfs in this directory to a local computer you can use the `scp` command.

```bash
$ scp summerschool@bestlapp.physics.uu.se:~/UuJetRecoProject/*.pdf .
```

All the files with the `.pdf` ending will then be saved to the current directory on the computer where the command was executed.

### About the code
To complete the project you have to modify the code. Open the file `jetReco.cxx` in a text editor, for example `gedit`, `vim`, or `emacs`.
```bash
$ gedit jetReco.cxx &
```

The file contains a bunch of `#include` statements at the top and a set of functions which performs the tasks we want to clusters and plots the jets.

At the bottom of the file is the `main` function. This is the main entry point of the program. Here we open the input file and read in the data we need for the clustering. The input file contains many events, but we will only select one specific event to use in the jet clustering. This is done by calling the `get_event()` functions, which takes a tree and event number as input, and fetches the cluster data for us. For example `get_event(tree, 0);` would get event 0 from the tree `tree`, `get_event(tree, 1)` would get event 1, and so on. To perform the clustering we call the function `cluster_jets()` which takes an event object as input. The event object gets filled with the cluster data by the already mentioned `get_event()` function.

The `cluster_jets()` function is where most of your changes will go. Be sure to read though it, and look at the comments, to get an idea of what it does. Here we define the radius parameter for the jet clustering and which algorithm to use. This is also where the clustering takes place, and where we call the functions to plot the result. Any action we want to perform on the jets or clusters, such as splitting and adding radiation, will take place in this function.

### Exercise 1

The goal is to study three common sequential recombination algorithms, kt, anti-kt, and Cambridge/Aachen. A good overview of their definition is given in section 4.1-4 of the fastjet user manual <http://fastjet.fr/repo/fastjet-doc-3.3.0.pdf>. Note that each algorithm has an inclusive and exclusive variant, we will only concern ourselves with the *inclusive* variant.

1. Start by looking at a few different events. This can be done where we call `get_event(tree, 10)` in the `main` function by changing the `10` to some other number. Try a few different numbers until you find an event you wish to use for the remaining tasks. Events 1, 2, 10, 11, 21, and 22 are pretty interesting, but feel free to check other numbers as well. Be sure to compile the code after every change.
2. See what happens when you change the jet algorithm in the `cluster_jets` function. Try the anti-kt, kt, and Cambrige/Aachen algorithms. How does the shape of the jet look for each algorithm? Try to think about why the shapes are as they are from the definition of each algorithm.
3. The ATLAS detector is a very busy environment. Protons are not collided 1-by-1, they come in bunches with about 100 billion protons per bunch. When a proper proton-proton collision occurs, it's likely to be contaminated from activity elsewhere in the bunch crossing, this is referred to as pile-up. Investigate how the pile-up affects the jets for the different algorithms, you can do this by using the `add_radiation` function.

### Exercise 2

If there's time left after Exercise 1, we can attempt some simple boosted top quark reconstruction. Do to this we need the notions of *jet trimming* and *jet substructure*. The skeleton code for this part is in `jetAna.cxx`.

Jet trimming is a way to suppress contamination from pile-up and the underlying event in the jet reconstruction. It works by reclustering the jet constituents using the kt algorithm, with a radius parameter of 0.2, into sub-jets. Then every sub-jet which carries a less than 5% of the original jets momentum are removed and the jet kinematics is recalculated. In `fastjet` this is done by using an instance of the `fastjet::Filter` class.

Jet substructure describes the internal structure of a jet. There are many substructure variables. One of which is N-subjettiness which is very useful for describing jets initiated by top quarks. N-subjettiness describes how well a jet can be described as being composed of N subjets (how N-subjetty it is). Lower values for a particular N means better agreement. In `fastjet` N-subjettiness is calculated with an instance of the `fastjet::contrib::Nsubjettiness` class.

We are concerned with the decay t → bW(qq), i.e. we will have 1 b- and 2 light (u, d, s, or c) quarks. Then it is reasonable to assume that jets containing the top quark decay will have low values of 3-subjettiness (one sub-jet for each quark). However other (background) jets (gluon- or QCD-jets) could also have low 3-subjettiness, so this variable alone is not enough to properly identify the signal (top quark) jets. But if we also consider 2-subjettiness, it will be high for signal jets, but for background jets this variable will be about the same as 3-subjettiness. So it turns out that taking the ratio of 3-subjettiness to 2-subjettiness is a very powerful variable for identifying jets containing hadronically decaying top quarks. It could be instructive to plot the trimmed jet mass vs 1-, 2-, and 3-subjettiness, and vs the ratio between 3- and 2-, and 2- and 1-subjettiness, to convince yourselves of this.

Another useful quantity is the opening angle ΔR of the top quark decay. A good approximation is ΔR = 2m/pT, where m is the mass of the decaying particle (the top in this case), and pT is it's transverse momentum.

The `make` command also produces a binary file `jetAna` which contain this program. It can be run in the same way as `jetReco`, but specify `mc15_13TeV.410506.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_allhad_boosted.merge.DAOD_TOPQ1.e5634_s2726_r7772_r7676_p2952.root` as the input file.

1. Make a collection of trimmed jets. Start by studying the `main` function in `jetAna.cxx`. It contains a loop over all events in the input file, where we start by making a collection of all the ungroomed jets in the same way as in `jetReco.cxx`. In the code there is also defined a `trimmer` object, use this to copy the jets in the ungroomed collection into a collection of trimmed jets which we will use in our simple top tagger.

2. Define the pT cut of our tagger. Use the formula for the opening angle of the top decay with appropriate values of ΔR and m to approximate the minimum pT a jet containing the top quark decay should have. Refine this guess by plotting jet mass vs. jet pT in a 2D histogram for the two highest pT jets. This should also contain some other interesting features, see if you can identify them.

3. Define the cut on the N-subjettiness ratio. There's no simple approximation for this one, but you can again plot the jet mass vs. this ratio for the two highest pT jets and find an appropriate cut on this variable. This plot should also have some interesting features.

4. With the two cuts defined, make a histogram of the jet mass for the jets among the 2 hardest jets that passes the cuts. Hopefully you will see a nice mass distribution around the true value of the top mass.

5. (Bonus) Run you top tagger on the input file from Exercise 1. This file contains mainly QCD jets and should give quite different results!

## References
- fastjet: <http://fastjet.fr/repo/fastjet-doc-3.3.0.pdf>
- kt: <https://arxiv.org/abs/hep-ph/9305266>
- anti-kt: <https://arxiv.org/abs/0802.1189>
- C/A: <https://arxiv.org/abs/hep-ph/9707323>
- Jet trimming: <https://arxiv.org/abs/0912.1342>
- N-subjettiness: <https://arxiv.org/abs/1011.2268>
