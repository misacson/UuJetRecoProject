// STL
#include <iostream>
#include <vector>

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TH1D.h"
#include "TH2D.h"
#include "THStack.h"
#include "TCanvas.h"
#include "TColor.h"
#include "TStyle.h"
#include "TUUID.h"
#include "TApplication.h"
#include "TStyle.h"
#include "TLegend.h"

// Fastjet
#include "fastjet/ClusterSequence.hh"
#include "fastjet/tools/Filter.hh"
#include "fastjet/contrib/Nsubjettiness.hh"

#define MATH_PI 3.14159265359

struct Event {
    std::vector<double>* cluster_pt  = nullptr;
    std::vector<double>* cluster_eta = nullptr;
    std::vector<double>* cluster_phi = nullptr;
    std::vector<double>* cluster_m   = nullptr;
};

void set_address(TTree* tree, Event& event) {
    tree->SetBranchAddress("cluster_pt"  , &event.cluster_pt);
    tree->SetBranchAddress("cluster_eta" , &event.cluster_eta);
    tree->SetBranchAddress("cluster_phi" , &event.cluster_phi);
    tree->SetBranchAddress("cluster_m"   , &event.cluster_m);
}

void get_event(TTree* tree, const uint64_t entry) {
    tree->GetEntry(entry);
}

std::vector<fastjet::PseudoJet> make_pseudojets(const Event& event) {

    TLorentzVector p4;
    std::vector<fastjet::PseudoJet> pjets;
    for (size_t i = 0; i < event.cluster_pt->size(); ++i) {
        p4.SetPtEtaPhiM(
                event.cluster_pt->at(i),
                event.cluster_eta->at(i),
                event.cluster_phi->at(i),
                event.cluster_m->at(i));
        pjets.push_back(fastjet::PseudoJet(p4.Px(), p4.Py(), p4.Pz(), p4.E()));
    }

    return pjets;
}

int main(int argc, const char* argv[]) {

    if (argc < 2) {
        printf("Usage: %s <inputfile>\n", argv[0]);
        return 1;
    }

    TApplication app("app", 0, 0);

    // gStyle->SetOptStat(0);
    gStyle->SetTitleFont(42, "xyz");
    gStyle->SetLabelFont(42, "xyz");
    gStyle->SetLegendFont(42);

    // Open the input file and fetch the tree containing event data
    TFile f(argv[1], "READ");
    TTree* tree = dynamic_cast<TTree*>(f.Get("nominal"));

    // Create an event object to hold the
    // cluster data, and attach it to the
    // data tree
    Event event;
    set_address(tree, event);

    // Set up the jet algorithm
    const double R = 1.0;
    const fastjet::JetAlgorithm        jet_algorithm = fastjet::antikt_algorithm;
    const fastjet::RecombinationScheme recomb_scheme = fastjet::E_scheme;
    const fastjet::Strategy            strategy      = fastjet::Best;
    fastjet::JetDefinition jet_def(jet_algorithm, R, recomb_scheme, strategy);

    const double x = 7.e3; // energy cut in MeV

    // Set up the trimmer
    // To get a trimmed jet, from an untrimmed (or ungroomed) jet, we use the
    // trimmer object as a function:
    //      fastjet::PseudoJet jet = trimmer(ungroomed_jet);
    // We can then save the jet in some container jets with the push_back() method:
    //      jets.push_back(jet);
    const fastjet::Filter trimmer(fastjet::JetDefinition(fastjet::kt_algorithm, 0.2), fastjet::SelectorPtFractionMin(0.05));

    // N-subjettines
    // To get e.g. the 3-subjettiness for a jet, do
    //      double tau3 = nSub3(jet)/1.e3;
    // We divide by 1000 to keep the units in GeV rather than MeV
    fastjet::contrib::Nsubjettiness nSub1(1, fastjet::contrib::OnePass_WTA_KT_Axes(), fastjet::contrib::UnnormalizedMeasure(1.0)); // 1-subjettiness
    fastjet::contrib::Nsubjettiness nSub2(2, fastjet::contrib::OnePass_WTA_KT_Axes(), fastjet::contrib::UnnormalizedMeasure(1.0)); // 2-subjettiness
    fastjet::contrib::Nsubjettiness nSub3(3, fastjet::contrib::OnePass_WTA_KT_Axes(), fastjet::contrib::UnnormalizedMeasure(1.0)); // 3-subjettiness

    // Some histograms you can use to plot event distributions
    TH1::SetDefaultSumw2(1);

    TH1D* h_ungroomed_jet_pt  = new TH1D("ungroomed_jet_pt", ";Jet p_{T}", 100, 0, 1000); // ungroomed jet pt

    TH1D* h_jet0_pt = new TH1D("jet0_pt", ";Leading jet p_{T}",     100, 0, 1000); // leading jet pt
    TH1D* h_jet1_pt = new TH1D("jet1_pt", ";Sub-leading jet p_{T}", 100, 0, 1000); // sub-leading jet pt
    TH1D* h_jet_pt  = new TH1D("jet_pt",  ";Jet p_{T}",             100, 0, 1000); // jet pt

    TH1D* h_jet0_m  = new TH1D("jet0_m",  ";Leading jet mass",      100, 0, 300); // leading jet mass
    TH1D* h_jet1_m  = new TH1D("jet1_m",  ";Sub-leading jet mass",  100, 0, 300); // sub-leading jet mass
    TH1D* h_jet_m   = new TH1D("jet_m",   ";Jet mass",              100, 0, 300); // jet mass

    TH2D* h_jet_m_vs_pt    = new TH2D("jet_m_vs_pt",    ";jet p_{T};jet mass", 100, 0, 1000, 100, 0, 300);
    TH2D* h_jet_m_vs_tau32 = new TH2D("jet_m_vs_tau32", ";tau_{32};jet mass",  100, 0, 1.2,  100, 0, 300);
    TH2D* h_jet_m_vs_tau21 = new TH2D("jet_m_vs_tau21", ";tau_{21};jet mass",  100, 0, 1.2,  100, 0, 300);

    TH2D* h_jet_m_vs_tau1 = new TH2D("jet_m_vs_tau1", ";tau_{1};jet mass",  100, 0, 200,  100, 0, 300);
    TH2D* h_jet_m_vs_tau2 = new TH2D("jet_m_vs_tau2", ";tau_{2};jet mass",  100, 0, 200,  100, 0, 300);
    TH2D* h_jet_m_vs_tau3 = new TH2D("jet_m_vs_tau3", ";tau_{3};jet mass",  100, 0, 200,  100, 0, 300);

    TH1D* h_tau32 = new TH1D("tau32", ";#tau_{32}", 100, 0, 1.2);
    TH1D* h_tau21 = new TH1D("tau21", ";#tau_{21}", 100, 0, 1.2);

    TH1D* h_tau1 = new TH1D("tau1", ";#tau_{1}", 100, 0, 300);
    TH1D* h_tau2 = new TH1D("tau2", ";#tau_{2}", 100, 0, 300);
    TH1D* h_tau3 = new TH1D("tau3", ";#tau_{3}", 100, 0, 300);

    // Begin event loop
    uint64_t nentries = tree->GetEntries();
    for (uint64_t i = 0; i < nentries; ++i) {
        get_event(tree, i);

        std::vector<fastjet::PseudoJet> pjets = make_pseudojets(event);
        fastjet::ClusterSequence cs(pjets, jet_def);
        std::vector<fastjet::PseudoJet> ungroomed_jets = fastjet::sorted_by_pt(cs.inclusive_jets(x));

        if (i % 1000 == 0) printf("%llu / %llu\n", i, nentries);

        // Example: make a histogram of the leading ungroomed jet pt, divide by 1000 to convert MeV->GeV
        h_ungroomed_jet_pt->Fill(ungroomed_jets[0].pt()/1.e3);

        // Define a container to hold our trimmed jets
        std::vector<fastjet::PseudoJet> jets;

        // Loop through all jets
        for (auto ungroomed_jet : ungroomed_jets) {
            // Trim the jets here
        }

        // We only care about events with at least 2 jets
        if (jets.size() < 2) continue;

        // Loop through only the two leading jets
        for (size_t j = 0; j < 2; ++j) {

            // For convenience we define the current jet as it's own variable, you can then
            // use this variable to get the jet properites. E.g. for getting the transverse
            // momentum you would call jet.pt(). Also remember that we plot energy
            // in GeV, but the jet stores all energies in MeV, so we need to divide
            // pt, mass, and other energy quantities by a factor 1000.
            const fastjet::PseudoJet& jet = jets[j];


            // Calculate the variables you're interested in here, e.g. N-subjettiness, pt, ...



            // Fill histograms which you are interested in



            // Define you simple tagger here. E.g.
            // if (/* jet passes some cuts */) {
            //      // Fill some histogram
            // }


        }


    } // End event loop

    // Just a helper function for plotting histograms
    auto draw_hist = [](TH1* h, const char* popt = ""){
        TString opt(popt);
        if (h->InheritsFrom("TH2")) opt += "colz";
        const char* cname = h->GetName();
        auto c = new TCanvas(cname, cname, 800, 600);
        c->SetLogz(1);
        h->Draw(opt);
    };

    // Plot your histograms, for 2D histograms use "colz" as the second argument
    // to get a better visualization
    draw_hist(h_ungroomed_jet_pt);

    app.Run();
}
